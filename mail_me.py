import smtplib
from secret import Secret
def send_mail():
    FROM = Secret.MAIL

    TO = [Secret.MAIL] # must be a list

    SUBJECT = "Znaleziono nowy termin u doktora"

    TEXT = "This message was sent with Python's smtplib."

    # Prepare actual message

    message = """\
    From: %s
    To: %s
    Subject: %s
    
    %s
    """ % (FROM, ", ".join(TO), SUBJECT, TEXT)

    # Send the mail

    server = smtplib.SMTP('smtp.gmail.com', 587)

    server.ehlo()
    server.starttls()
    server.ehlo()

    server.login(Secret.MAIL, Secret.MAIL_PASS)
    server.sendmail(FROM, TO, message)
    server.quit()