from selenium import webdriver
from selenium.webdriver.common.by import By
from time import sleep
from dateutil.relativedelta import relativedelta
import datetime
from secret import Secret


class Luxmed(object):
    def __init__(self):
        self.driver = webdriver.Firefox()

    def login(self, username, password):
        driver = self.driver
        driver.get("https://portalpacjenta.luxmed.pl/PatientPortal/Account/LogOn")
        assert driver.title == 'Portal Pacjenta LUX MED'
        driver.find_element_by_id("Login").send_keys(username)
        driver.find_element_by_id("TempPassword").send_keys(password)
        driver.find_element_by_class_name("submit").click()
        return driver

    def find_doctors_free(self, specialization, doctor_name):
        driver = self.driver
        sleep(3)
        assert driver.title == 'Portal Pacjenta LUX MED'
        assert driver.find_element_by_id("content") is not None
        driver.find_elements_by_class_name("caption")[3].click()
        driver.find_element_by_class_name("search-select").send_keys(specialization)
        spec = driver.find_elements_by_xpath("//li[text()='{}']".format(specialization))
        assert len(spec) == 1
        spec[0].click()
        sleep(2)
        driver.find_elements_by_class_name("caption")[4].click()
        driver.find_element_by_class_name("search-select").send_keys(doctor_name)
        doc = driver.find_elements_by_xpath("//li[text()='{}']".format(doctor_name))
        assert len(spec) == 1
        doc[0].click()
        sleep(1)
        driver.find_element_by_css_selector("input[class = 'button pretty'").click()
        return driver

    def get_free_terms(self):
        driver = self.driver
        sleep(10)
        free_terms = {} # day: [hours]
        days = driver.find_elements_by_class_name("title")
        for day in days:
            print(day.text)
            list_of_hours = []
            parent_tag = day.find_element_by_xpath('..')
            hours = parent_tag.find_elements_by_class_name("hours")
            for hour in hours:
                hh = hour.get_attribute('data-sort')
                if hh is not None:
                    list_of_hours.append(hh)
            print(day.text)
            day_name = day.text.split(',')[1].strip()
            free_terms[day_name] = list_of_hours

        self.free_terms = free_terms
        return free_terms

    def check_if_they_are_free_terms_n_days_from_now(self, days_from_now):
        recent_days = {}
        for day in self.free_terms:
            dd = datetime.datetime.strptime(day, "%d-%m-%Y").date()
            print((dd - datetime.date.today()).days)
            if (dd - datetime.date.today()).days <= days_from_now:
                recent_days[day] = self.free_terms[day]
        return recent_days





lx = Luxmed()
lx.login(Secret.LOGIN, Secret.PASSWORD)
lx.find_doctors_free("Konsultacja ginekologa", "PISKORZ TOMASZ lek. med.")
terms = lx.get_free_terms()
aa = lx.check_if_they_are_free_terms_n_days_from_now(60)